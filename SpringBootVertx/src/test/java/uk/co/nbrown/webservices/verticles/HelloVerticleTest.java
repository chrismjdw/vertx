package uk.co.nbrown.webservices.verticles;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import uk.co.nbrown.webservices.services.MessageService;

@RunWith(VertxUnitRunner.class)
public class HelloVerticleTest {

	private Vertx vertx;
	private HelloVerticle helloVerticle;
	
	@Before
	public void setup(TestContext context) {
		vertx = Vertx.vertx();
		helloVerticle = new HelloVerticle(new MessageService());
		vertx.deployVerticle(helloVerticle, context.asyncAssertSuccess());
	}
	
	@After
	public void tearDown(TestContext context) {
		vertx.close();
	}
	
	@Test
	public void givenPathWithoutNameWhenRequestLocalhostHelloThenReceiveStandardGreeting(TestContext context) {
		Async async = context.async();
		
		WebClient.create(vertx).get(HelloVerticle.PORT, "localhost", "/hello").send(
				asyncResult -> {
					context.assertTrue(asyncResult.succeeded());
					HttpResponse<Buffer> response = asyncResult.result();
					context.assertEquals("Hello, world!", response.body().toString());
					async.complete();
				});
	}
	
	@Test
	public void givenPathWithNameWhenRequestLocalhostHelloThenReceivePersonalGreeting(TestContext context) {
		Async async = context.async();
		
		WebClient.create(vertx).get(HelloVerticle.PORT, "localhost", "/hello/name").send(
				asyncResult -> {
					context.assertTrue(asyncResult.succeeded());
					HttpResponse<Buffer> response = asyncResult.result();
					context.assertEquals("Hello, name", response.body().toString());
					async.complete();
				});
	}
}
