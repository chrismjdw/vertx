package uk.co.nbrown.webservices.services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MessageServiceTest {

	private static final String NAME = "name";
	
	private MessageService messageService = new MessageService();
	
	@Test
	public void givenNoNameParameterWhenGetMessageThenGetStandardMessage() {
		assertEquals(MessageService.STANDARD_MESSAGE, messageService.getMessage());
	}
	
	@Test
	public void giveNameParameterWhenGetMessageThenGetPersonalMessage() {
		assertEquals(MessageService.PERSONAL_MESSAGE+NAME, messageService.getMessage(NAME));
	}
}
