package uk.co.nbrown.webservices;

import io.vertx.core.Vertx;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import uk.co.nbrown.webservices.verticles.HelloVerticle;

@SpringBootApplication
public class SpringBootVertxApplication {

	@Autowired
	private HelloVerticle helloVerticle;
	
	@PostConstruct
	public void deployVerticle() {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(helloVerticle);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootVertxApplication.class, args);
	}
}