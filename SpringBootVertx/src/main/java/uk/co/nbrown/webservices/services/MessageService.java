package uk.co.nbrown.webservices.services;

import org.springframework.stereotype.Service;

@Service
public class MessageService {

	protected static final String STANDARD_MESSAGE = "Hello, world!";
	protected static final String PERSONAL_MESSAGE = "Hello, ";
	
	public String getMessage() {
		return STANDARD_MESSAGE;
	}
	
	public String getMessage(String name) {
		return PERSONAL_MESSAGE + name;
	}
}
