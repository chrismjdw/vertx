package uk.co.nbrown.webservices.verticles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import uk.co.nbrown.webservices.services.MessageService;

@Component
public class HelloVerticle extends AbstractVerticle {
	
	protected static final int PORT = 8081;
	
	private MessageService messageService;
	
	@Autowired
	public HelloVerticle(MessageService messageService) {
		this.messageService = messageService;
	}
	
	@Override
	public void start(Future<Void> future) {
		Router router = Router.router(vertx);
		router.get("/hello").handler(this::getStandardGreeting);
		router.get("/hello/:name").handler(this::getPersonalGreeting);
		
		vertx.createHttpServer().requestHandler(router).listen(PORT,
			result -> {
				if(result.succeeded()) {
					future.complete();
				} else {
					future.fail(result.cause());
				}
			});
	}
	
	public void getStandardGreeting(RoutingContext routingContext) {
		routingContext.response().end(messageService.getMessage());
	}
	
	public void getPersonalGreeting(RoutingContext routingContext) {
		String name = routingContext.request().getParam("name");
		routingContext.response().end(messageService.getMessage(name));
	}
}