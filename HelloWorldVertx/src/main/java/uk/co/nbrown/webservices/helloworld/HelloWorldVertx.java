package uk.co.nbrown.webservices.helloworld;

import io.vertx.core.Vertx;

public class HelloWorldVertx {
	
	public static final int PORT = 8081;
	
	public static void main(String[] args) {
		Vertx.vertx()
			.createHttpServer()
			.requestHandler(req -> req.response().end("Hello World!"))
			.listen(PORT, handler -> {
				if(handler.succeeded()) {
					System.out.println("http://localhost:"+PORT+"/");
				} else {
					System.err.println("Failed to listen on port "+PORT);
					System.err.print(handler.cause());
				}
			});
	}
}